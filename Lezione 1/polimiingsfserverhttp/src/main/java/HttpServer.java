import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpServer {

    private ServerSocket serverSocket;
    private static final int PORT = 1234;

    public HttpServer() throws IOException{
        this.serverSocket = new ServerSocket(PORT);
    }

    public void run(){
        try {
            Socket socket = serverSocket.accept();
            new ServerFileYelder(socket).run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]){
        try {
            HttpServer httpServer = new HttpServer();
            httpServer.run();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
