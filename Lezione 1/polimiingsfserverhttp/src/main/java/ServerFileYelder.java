import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class ServerFileYelder {
    private Socket socket;

    public ServerFileYelder(Socket socket){
        this.socket = socket;
    }

    public void run(){
        try {
            File file = new File("./pippo");
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(socket.getOutputStream());
            outputStreamWriter.write(file.toString());
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
