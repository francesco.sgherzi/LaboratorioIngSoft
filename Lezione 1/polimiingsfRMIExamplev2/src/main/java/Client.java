import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {

    public static void main(String args[]){

        try {
            /*Adder stubReturned = (Adder) Naming.lookup("rmi://localhost:5000/adder");*/
            Registry registry = LocateRegistry.getRegistry(5000);
            Adder stub = (Adder) registry.lookup("adder");
            System.out.println(stub.add(23, 2));
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }

}
