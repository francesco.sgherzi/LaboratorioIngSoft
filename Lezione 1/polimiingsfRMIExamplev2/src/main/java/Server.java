import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    public static void main(String args[]){


        try {
            LocateRegistry.createRegistry(5000);
            Adder stub = new AdderRemote();
            /*Naming.rebind("rmi://localhost:5000/adder", stub);*/
            Registry registry = LocateRegistry.getRegistry(5000);
            registry.rebind("adder", stub);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}
