package server;

import client.ClientInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote {

    public void addClient(ClientInterface client) throws RemoteException;
    public void send(String message) throws RemoteException;

}
