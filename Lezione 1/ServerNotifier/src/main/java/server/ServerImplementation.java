package server;

import client.ClientInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ServerImplementation extends UnicastRemoteObject implements ServerInterface {


    protected ServerImplementation() throws RemoteException {
        super(0);
    }

    public void addClient(ClientInterface client) throws RemoteException {

    }

    public void send(String message) throws RemoteException {

    }
}
