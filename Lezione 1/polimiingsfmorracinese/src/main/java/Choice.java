public enum Choice {
    ROCK, PAPER, SCISSOR;

    public static String wins(Choice client, Choice server){

        if(client == PAPER && server == ROCK || client == ROCK && server == SCISSOR || client == SCISSOR && server == PAPER){
            return "client";
        } else if (server == PAPER && client == ROCK || server == ROCK && client == SCISSOR || server == SCISSOR && client == PAPER){
            return "server";
        }

        return "draw";

    }

    public static Choice parseInput(String choice){

        if(choice.toUpperCase().equals("ROCK")) return Choice.ROCK;
        if(choice.toUpperCase().equals("PAPER")) return Choice.PAPER;
        if(choice.toUpperCase().equals("SCISSOR")) return Choice.SCISSOR;

        return null;

    }

    public static Choice parseInput(int choice){



        if(Math.floor(choice) == 0) return Choice.ROCK;
        if(Math.floor(choice) == 1) return Choice.PAPER;
        if(Math.floor(choice) == 2) return Choice.SCISSOR;

        return null;

    }

}
