import java.io.IOException;

public class Main {

    public static final int PORT = 1234;

    public static void main(String args[]){
        try {
            Server mServer = new Server(PORT);
            mServer.instantiateConnection(Choice.parseInput(args[0]));
            mServer.writeChoice();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
