import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private int choice;
    private ServerSocket serverSocket;
    private Socket connection = null;

    public void instantiateConnection(Choice clientChoice) throws IOException {
        this.connection = this.serverSocket.accept();
        Client pippo = new Client(this.connection, clientChoice);
        pippo.instatiateConnection();
    }

    public void writeChoice() throws IOException {
        if (this.connection == null) throw new IOException("Connection not found");
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.connection.getOutputStream());
        outputStreamWriter.write(this.choice);
    }

    public Server(int port) throws IOException{

        this.serverSocket = new ServerSocket(port);
        this.choice = (int) (Math.random() * 3 - 0.0000001);
    }

}
