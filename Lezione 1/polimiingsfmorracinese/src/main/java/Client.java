import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private Socket socket;
    private InputStreamReader isReader;
    private Choice choice;

    public void instatiateConnection() throws IOException{
        this.isReader = new InputStreamReader(this.socket.getInputStream());
        while(isReader.ready()){
            Choice serverChoice = Choice.parseInput(isReader.read());
            if(serverChoice != null){
                System.out.println(Choice.wins(this.choice, serverChoice) + " wins!");
            }

        }
    }

    public Client(Socket socket, Choice choice){
        this.socket = socket;
        this.choice = choice;
    }

}
