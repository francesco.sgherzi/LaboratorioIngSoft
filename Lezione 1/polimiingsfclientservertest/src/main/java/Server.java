import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static final int PORT = 1234;
    private ServerSocket serverSocket;
    private Socket connection = null;

    public Server(int port) throws IOException {
        this.serverSocket = new ServerSocket(port);
    }

    public void run() throws IOException {
        this.connection = this.serverSocket.accept();
        new Client(this.connection).run();
    }

    public void writeSomething(String something) throws IOException{
        if(this.connection == null){
            throw new IOException("Connection not initialized");
        }
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.connection.getOutputStream());
        outputStreamWriter.write(something);
        outputStreamWriter.close();
    }

    public static void main(String args[]){
        try {

            Server mServer = new Server(PORT);
            mServer.run();
            mServer.writeSomething(args[0]);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
