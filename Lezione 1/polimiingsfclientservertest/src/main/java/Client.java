import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client {

    private Socket socket;
    StringBuilder message;

    public Client(Socket socket){
        this.socket = socket;
    }

    public void run() throws IOException {
        InputStreamReader isreader = new InputStreamReader(this.socket.getInputStream());
        while(isreader.ready()){
            message.append(isreader.read());
        }
        System.out.println(message);
    }

}
