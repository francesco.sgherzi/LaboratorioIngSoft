import java.io.*;
import java.util.Date;

public class Serializeit {

    ByteArrayOutputStream os = null;

    public boolean serializeAndSend(Object object) {

        try {
            os = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);
            oos.writeObject(object);
        } catch (IOException e) {
            /*e.printStackTrace();*/
            return false;
        }
        return true;
    }

    public boolean reciveAndDeserialize(){

        try {
            ByteArrayInputStream is = new ByteArrayInputStream(os.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(is);
            Person deserializedObj= (Person) ois.readObject();
            System.out.println(deserializedObj.toString());
        } catch (IOException e) {
            /*e.printStackTrace();*/
            System.out.println("IOExeption");
            return false;
        } catch (ClassNotFoundException e) {
            /*e.printStackTrace();*/
            System.out.println("ClassNotFoundExeption");
            return false;
        }

        return true;
    }

    public static void main(String args[]){

        Person Mario = new Person("Mario", new Date());
        PersonNotSerializable notSerializableMario = new PersonNotSerializable("Mario", new Date());

        Serializeit serializeit = new Serializeit();

        assert serializeit.serializeAndSend(Mario) : "Should serialize and send Person, which is serializable";
        /*assert !serializeit.serializeAndSend(notSerializableMario) : "Sould NOT serialize and send Date, which is not serializable";
*/
        serializeit.reciveAndDeserialize();

    }

}
