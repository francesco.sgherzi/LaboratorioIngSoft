import java.io.Serializable;
import java.util.Date;

public class PersonNotSerializable{ // A serializable obj MUST implement the Serializable Interface


    private String name;
    private Date birthday;

    public PersonNotSerializable(String name, Date birthday){
        this.name = name;
        this.birthday = birthday;

    }

}
