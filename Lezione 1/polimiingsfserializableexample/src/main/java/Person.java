import java.io.Serializable;
import java.util.Date;

public class Person implements Serializable{ // A serializable obj MUST implement the Serializable Interface

    public static final long serialVersionUID = 13247383827892L; // MUST have a serialVersionUID in order to check for compatibility btw versions

    private String name;
    private Date birthday;

    public Person(String name, Date birthday){
        this.name = name;
        this.birthday = birthday;

    }

    @Override
    public String toString() {
        return "Name: " + this.name + "\n" + "Birthday: " + this.birthday.toString();
    }
}
