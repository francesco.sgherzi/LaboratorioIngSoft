import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class VTwoClient {

    public static void main(String args[]){

        try {

            Registry registry = LocateRegistry.getRegistry(12345);
            Adder stub = (Adder) registry.lookup("adder");
            System.out.println(stub.add(23, 1));

        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }

    }

}
