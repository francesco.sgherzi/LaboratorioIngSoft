import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Client {

    public static void main(String args[]){

        try {
//            ClientInterface stub = (ClientInterface) Naming.lookup("rmi://localhost:12345/sayHello");

            Registry reg = LocateRegistry.getRegistry("localhost", 12345);
            ClientInterface stub = (ClientInterface) reg.lookup("sayHello");
            ClientInterface stubv = (ClientInterface) reg.lookup("sayGoodbye");

            System.out.println(stub.say());
            System.out.println(stubv.say());
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }


}
