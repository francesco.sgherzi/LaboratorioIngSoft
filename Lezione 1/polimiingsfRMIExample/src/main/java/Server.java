import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Locale;

public class Server {

    public static void main(String args[]){
        try {
            ClientInterface stubHello = new ClientImpl();
            ClientInterface stubGoodbye = new ClientImplv();
            /*Naming.rebind("rmi://localhost:12345/sayHello", stub);*/
            Registry reg = LocateRegistry.getRegistry(12345);
            reg.rebind("sayHello", stubHello);
            reg.rebind("sayGoodbye", stubGoodbye);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}
