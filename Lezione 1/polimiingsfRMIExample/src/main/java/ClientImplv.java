import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ClientImplv extends UnicastRemoteObject implements ClientInterface{


    protected ClientImplv() throws RemoteException {
        super();
    }

    public String say() throws RemoteException {
        return "GoodBye there";
    }
}
