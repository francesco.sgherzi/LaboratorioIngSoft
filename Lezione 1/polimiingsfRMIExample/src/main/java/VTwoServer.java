import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class VTwoServer {

    public static void main(String args[]){

        try {

            AdderImpl stub = new AdderImpl();
            Registry registry = LocateRegistry.getRegistry(12345);
            registry.rebind("adder", stub);

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

}
