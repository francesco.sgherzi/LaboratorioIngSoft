# Steps to implement RMI
## Server side
* Define a remote interface to be sent. It has to extend the `Remote` interface and every method must throw a remoteExeption

* Define a `Server` class that implements the interface to be sent, implement the methods.

* Instantiate the `Server` class.

* Create a `stub` of the same type of the interface to be sent and set it equal to `(<typeOfInterface>) UnicastRemoteObject.exportObject(<instantiateObj>, portnumber)` 

* Bind the remote object's stub in the registry giving it a name and passing the stub

## Client side

* get the registry using `LocateRegistry.getRegistry()`

* do a lookup (`registry.lookup(<NameInsertedbefore>)`) and cast it to the type sended 

* exec methods remotely and... PROFIT
