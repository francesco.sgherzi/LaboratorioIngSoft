import jdk.nashorn.internal.runtime.regexp.joni.Regex;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CSVReader implements CSVReaderInterface{

    private String[] keys;
    private ArrayList<String[]> values;

    public void load(){

        try {
            File mInputFile = new File("./mock.csv"); // declare a new inputFile
            InputStream mInputStream = new FileInputStream(mInputFile); // declare a new inputStream
            // BufferedReader mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream));
            Scanner mScanner = new Scanner(mInputStream); // Scanner più utile perché tanto devo andare solo avanti, non indietro

            String[] lineToPrint;
            keys = mScanner.nextLine().split(",");
            values =  new ArrayList<String[]>();

            while(mScanner.hasNext()){

                lineToPrint = mScanner.nextLine().trim().split(",");
                values.add(lineToPrint);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[]) {

        CSVReader csvr = new CSVReader();
        csvr.load();

        System.out.println(csvr.size());

    }

    public Map<String, String> getRow(int index) {
        Map<String, String> mMap = new HashMap<String, String>();

        String[] reference = values.get(index);

        for(int i = 0; i < keys.length; i++){
            mMap.put(keys[i], reference[i]);
        }

        return mMap;
    }

    public int size() {
        return values.size();
    }
}
