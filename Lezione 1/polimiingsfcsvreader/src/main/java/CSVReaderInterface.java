import java.util.Map;

public interface CSVReaderInterface {
    public Map<String, String> getRow(int index);
    public int size();
}
