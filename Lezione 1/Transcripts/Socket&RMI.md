# Socket
## UDP vs TCP
2 protocolli di comunicazione
* TCP - Transmission Data Protocol => Orientato alla connessione, controlla la connessione (Nel progetto si usa questo)
* UDP - User Datagram Protocol =>  Pacchetti vengono scambiati #accazzo

Il protocollo dipende dall'applicazione. Per bassa latenza viene utilizzato UDP, altrimenti si usa TCP.

## Socket in Java
### Socket
Si utilizzano per connettersi a un ServerSocket.
```
Socket echoSocket = new Socket(hostName, port); //Creates a new socket against `${hostname}:${port}`
PrintWriter out = new PrintWriter(echoSocket.getOutputStream(), true); // Creates a new PrintWriter obj with outoflushing disabled
BufferedReader in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream())); // Creates a new input buffer using the Buffered Reader class
BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in)); // Creates a new inputBuffer to stdin

```

close() chiude un socket aperto. Se ci so no eventuali operazioni in corso vengono bloccate con IOException.

### ServerSocket
Resta in ascolto su una certa porta.
Per ottenere il socket bisogna lanciare accept(), su cui poi si può effettuare lo scambio dati
```
ServerSocket serverSocket = new ServerSocket(Integer.parseInt(args[0])); // Creates a ServerSocket on port args[0]
Socket clientSocket = serverSocket.accept();     // Accepts connections
PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);  // Opens an Output Stream
BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream())); // Opens an Input Stream

```
Tutti i metodi sono bloccanti.
Lo svantaggio è che il server resta in attesa finché non arriva il timeout della connessione, però nel frattempo il server non riesce a gestire ulteriori connessioni da altri client.


# RMI
2 tipi di oggetti
* Oggetti remoti => ho riferimento al puntatore
* Oggetti non remoti => passati tramite serializzazione

**NB: RMI supporta il download automatico delle classi**

Il server crea oggetti remoti, li rende visibili e attende che i client li usino.
Il client ottiente i riferimenti e invoca metodi su di essi.

## Caratteristiche di RMI
* Localizzazione di oggetti remoti => posso associare ad ogni oggetto una stringa con i quali posso richiamarli
* Comunicazione con oggetti remoti => si invocano metodi come se gli oggetti fossero locali, e RMI se la gestisce
* Dynamic class loading => RMI può caricare codice degli oggetti runtime

## Definizioni

* Oggetti remoto => oggetti i quali metodi possono essere chiamati anche da una JVM diversa
* Interfaccia remota => specifica quali metodi possono essere invocati da una diversa JVM
* Server => insieme di uno o più oggetti che offrono risorse a macchine esterne sulla rete
* Remote Method Invocation => Processo di invocazione di un metodo presente su un interfaccia remota

## Registry
()
Permette di associare un generico oggetto ad una stringa
`LocateRegistry.createRegistry()`
oppure da terminale (INSERIRE COME).

## Architettura

* Stub => (Inserire roba) <!--//TODO completare Stub -->
* Skeleton => rappresenta il server lato client

## Interfaccia remota
L'interfaccia deve essere pubblica, deve estendere Remote, ogni metodo deve dichiarare una RemoteException

## Lato Server
* Devo inizializare il mio registry (come visto al p. Registry)
* Instanzio l'oggetto
* Creo oggetto Stub tramite `UncastRemoteObject`
*

## Lato client
<!-- // TODO riempire con cose -->
* Prendo il riferimento presente sul server al RMI
* prendo il riferimento all'oggetto stub usando Registry








.
