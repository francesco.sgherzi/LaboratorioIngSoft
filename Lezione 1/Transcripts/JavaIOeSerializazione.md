# JavaI/O e serializzazione (qui ci scappa il morto)

 * Java distingue tra trasmissione di byte e caratteri

## Input Stream
Permette di gestire flussi di dati in ingresso, che entrano nel programma.
`int available()` => Restituisce il numero di byte presenti nello stream.
`void close()` => Chiude il flusso di dati in ingresso.
`int read()` => Permette di leggere un byte in ingresso. è metodo bloccante e aspetta finché il byte è disponibile e lo restituisce come int.
`int read(byte[] b)` => Legge un certo numero di dati e li mette nell'array. Il programma viene bloccato fino a che non ci sono dati disponibili.
`int read(byte[] b, int off, int len)` => Permette di specificare da che punto partire nell'array e quanti byte leggere.
`long skip(long n)` => Salta senza leggere un certo numero di byte

## Output Stream
Offre dei metodi che permettono l'output di dati.

`void close()` => Chiude lo stream.
`void flush()` => Metodo particolare che garantisce che i dati siano effettivamente scritti (pulisce la cache/buffer).
`void write(int b)` => Scrive effettivamente i dati nello stream
`void write(byte[] b)` =>
`void write(byte[] b, int off, int len)` =>


## FileInputStream e FileOutputStream
Sono due implementazioni di classi che permettono di fare input / output su file.
Il parametro append permette di scegliere se partire dall'inizio o dalla fine del file


## ByteArrayInputStream e ByteArrayOutputStream
Permette di trattare un array di byte come un input
`ByteArrayInputStream(byte[] buf)` => buf contiene effettivamente i dati

## BufferedInputStream e BufferedOutputStream
Aggiunge mark e reset, cioè con mark segno un punto del file e con reset torno indietro a quel punto.

## DataInputStream e DataOutputStream
Aggiunge funzionalità ad un input Stream, come ad esempio leggere/scrivere caratteri, stringhe, numeri ecc.
**NB i metodi operano in binario**

## Reader e Writer
Accesso al flusso di dati tramite caratteri (o stringhe) e non byte.
Molto simile a FileInputStream/FileOutputStream

es
```
void read(char[] cbuf)
void write(char[] cbuf)
```


InputStreamWriter e OutputStreamWriter fanno da ponte fra lettura di caratteri e di byte

## Scanner
La classe scanner fornisce un input ricco, trattando l'input sempre come se fosse testo, permette il parsing di boolean, float, int...

## PrintWriter
Permette di scrivere vari valori in output, dando la rappresentazione


# Serializzazione
Permette di salvare strutture dati in modo automatico.

## Cosa può essere serializzato?
* Tipi Primitivi
* Stringhe
* Istanze di classe Serializabili
## Non possono essere serializzati
* Riferimenti
* Risorse di sistema => rappresentano una astrazione del sistema, non tanto dati che si possono inviare...

**NB: la serializzazione copia i dati, non i riferimenti**
**NB: con la deserializzazione otteniamo nuovi oggetti (con riferimenti diversi) ma con lo stesso contenuto**

**NB: Per essere serializzabile una classe deve implementare l'interfaccia Serializable e/o contenere solo roba serializabile**

_serialVersionUID identifica la versione della classe serializzata_
## Come serializzo
Per serializzare basta chamare writeObject() della calsse ObjectOutputStream
## Come deserializzo
readObject() di ObjectInputStream

**NB: Nel confronto di due oggetti con == esso restituirà false perché confronta i riferimenti e la serializzazione non conserva i riferimenti .equals(), invece, restituirà true**
### Errori

* ClassNotFoundExeption => Se la classe _modello_ non è trovata nel path (c'è qualcosa da deserializzare ma non so che oggetto sia, come rappresentarlo in memoria)

Gli attributi transient non vengono serializzati e vengono caricati come null















.
