import controller.StudentController;
import model.Student;
import view.StudentView;

public class Main {
    public static void main(String args[]) {

        Student model = retriveStudentsFromDB();
        StudentView view = new StudentView();
        StudentController controller = new StudentController(model, view);
        controller.updateView();
        controller.setStudentName("John");
        controller.updateView();

    }

    private static Student retriveStudentsFromDB() {
        return new Student("123", "Robert");
    }
}
