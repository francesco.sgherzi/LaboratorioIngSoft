package test;

import dollarfortest.Dollar;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Random;


public class TestMultiplication {

    @Before
    public void init(){

    }

    @After
    public void cleanup(){

    }

    @Test
    public void test() {
        Dollar dollar = new Dollar(0);

        Dollar result = dollar.times(0);

        Assert.assertEquals(0, result.getAmount());


    }

    @Test
    public void testRandom(){
        Dollar dollar = new Dollar(new Random(0));
        Dollar result = dollar.times(0);
        Assert.assertEquals(232, result);
    }

}
