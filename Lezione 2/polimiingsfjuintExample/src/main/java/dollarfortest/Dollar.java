package dollarfortest;

import java.util.Random;

public class Dollar {
    private int amount = 0;

    public Dollar(Random random){
        this.amount =  random.nextInt();
    }

    public Dollar(int amount) {
        this.amount = amount;
    }

    public Dollar times(int value){
        return new Dollar(amount * value);
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
