package it.polimi.deib.se.pf02.fullmvc.model;

public enum Outcome {
	WINNER("Hai vinto!"), LOSER("Hai perso!"), DRAW("Parità!");
	private final String message;

	private Outcome(String message){
		this.message = message;
	}
	
	@Override
	public String toString() {
		return message;
	}
	
	public static Outcome winsIfTrue(boolean condition){
		return (condition)?Outcome.WINNER:Outcome.LOSER;
	}


}
