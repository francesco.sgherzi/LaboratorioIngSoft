package it.polimi.deib.se.pf02.trismvc.server;

import it.polimi.deib.se.pf02.trismvc.controller.Controller;
import it.polimi.deib.se.pf02.trismvc.model.Cell;
import it.polimi.deib.se.pf02.trismvc.model.Model;
import it.polimi.deib.se.pf02.trismvc.model.Player;
import it.polimi.deib.se.pf02.trismvc.view.RemoteView;
import it.polimi.deib.se.pf02.trismvc.view.View;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static final int PORT = 12345;

    private ServerSocket serverSocket;

    private ExecutorService executor = Executors.newFixedThreadPool(128);

    private Map<String, ClientConnection> waitingConnection = new HashMap<>();

    private Map<ClientConnection, ClientConnection> playingConnection = new HashMap<>();


    /*
     * Deregistro una connessione
     */
    public synchronized void deregisterConnection(ClientConnection c) {
        ClientConnection enemy = playingConnection.get(c);
        if(enemy != null) {
            enemy.closeConnection();
        }
        playingConnection.remove(c);
        playingConnection.remove(enemy);
        Iterator<String> iterator = waitingConnection.keySet().iterator();
        while(iterator.hasNext()){
            if(waitingConnection.get(iterator.next())==c){
                iterator.remove();
            }
        }
    }

    /*
     * Mi metto in attesa di un altro giocatore
     */
    public synchronized void rendezvous(ClientConnection c, String name){
        waitingConnection.put(name, c);
        if (waitingConnection.size() == 2) {
            List<String> keys = new ArrayList<>(waitingConnection.keySet());
            ClientConnection c1 = waitingConnection.get(keys.get(0));
            ClientConnection c2 = waitingConnection.get(keys.get(1));
            View player1 = new RemoteView(new Player(keys.get(0), Cell.X), keys.get(1), c1);
            View player2 = new RemoteView(new Player(keys.get(1), Cell.O), keys.get(0), c2);
            Model model = new Model();
            Controller controller = new Controller(model);
            model.register(player1);
            model.register(player2);
            player1.register(controller);
            player2.register(controller);
            playingConnection.put(c1, c2);
            playingConnection.put(c2, c1);
            waitingConnection.clear();
        }
    }

    public Server() throws IOException {
        this.serverSocket = new ServerSocket(PORT);
    }

    public void run(){
        while(true){
            try {
                Socket newSocket = serverSocket.accept();
                SocketClientConnection socketConnection = new SocketClientConnection(newSocket, this);
                executor.submit(socketConnection);//Equivalente a new Thread(c).start();
            } catch (IOException e) {
                System.out.println("Errore di connessione!");
            }
        }
    }

    public static void main(String[] args) {
        Server server;
        try {
            server = new Server();
            server.run();
        } catch (IOException e) {
            System.err.println("Impossibile inizializzare il server: " + e.getMessage() + "!");
        }
    }

}