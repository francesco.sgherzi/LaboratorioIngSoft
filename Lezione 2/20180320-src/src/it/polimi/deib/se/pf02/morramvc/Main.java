package it.polimi.deib.se.pf02.morramvc;

import it.polimi.deib.se.pf02.morramvc.controller.Controller;
import it.polimi.deib.se.pf02.morramvc.model.Model;
import it.polimi.deib.se.pf02.morramvc.model.ModelView;
import it.polimi.deib.se.pf02.morramvc.view.View;

public class Main {
	
	private Model model;
	
	private ModelView modelView;
	
	private View view;
	
	private Controller controller;
	
	public Main() {		
		model = new Model();
		modelView = new ModelView();
		view = new View(System.in, System.out);		
		controller = new Controller(model, view);
		view.addObserver(controller);
		model.addObserver(modelView);
		modelView.addObserver(view);
		
	}
	
	public static void main(String[] args) {
		Main main = new Main();
		main.run();		
	}

	private void run() {		
		view.run();		
	}	

}
