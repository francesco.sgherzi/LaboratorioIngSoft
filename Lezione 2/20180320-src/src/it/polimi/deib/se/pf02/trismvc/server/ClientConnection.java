package it.polimi.deib.se.pf02.trismvc.server;

import it.polimi.deib.se.pf02.trismvc.utils.Observer;

public interface ClientConnection{

    void closeConnection();

    void asyncSend(String message);

    void register(Observer<String> observer);

}
