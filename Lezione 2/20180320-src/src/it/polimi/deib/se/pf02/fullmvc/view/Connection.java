package it.polimi.deib.se.pf02.fullmvc.view;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Connection extends Observable<String> implements Runnable {
	
	private Socket socket;
	
	private Scanner in;
	
	private PrintStream out;
	
	private Server server;
	
	private String name;
	
	private boolean active = true;
	
	public Connection(Socket socket, Server server) {
		this.socket = socket;
		this.server = server;					
	}
	
	private synchronized boolean isActive(){
		return active;
	}
	
	@Override
	public void run() {
		try{
			in = new Scanner(socket.getInputStream());
			out = new PrintStream(socket.getOutputStream());
			send("Benvenuto! Chi sei?");
			String read = in.nextLine();
			name = read;
			server.rendezvous(this, name);
			while(isActive()){
				read = in.nextLine();				
				notify(read);
			}			
		} catch (IOException | NoSuchElementException e) {
			System.err.println("Errore!");
		}finally{
			close();
		}		
	}
	
	public void send(String message) {
		out.println(message);
		out.flush();
	}
	
	public void asyncSend(final String message){
		new Thread(new Runnable() {			
			@Override
			public void run() {
				send(message);				
			}
		}).start();
	}
	
	public synchronized void closeConnection() {		
		send("Connessione terminata!");
		try {
			socket.close();
		} catch (IOException e) {
		}
		active = false;
	}
	
	private void close() {
		closeConnection();		
		System.out.println("Deregistro il client!");
		server.deregisterConnection(this);
	}

}
