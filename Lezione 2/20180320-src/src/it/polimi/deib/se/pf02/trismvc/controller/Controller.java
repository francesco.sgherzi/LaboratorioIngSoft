package it.polimi.deib.se.pf02.trismvc.controller;

import it.polimi.deib.se.pf02.trismvc.model.Model;
import it.polimi.deib.se.pf02.trismvc.model.PlayerMove;
import it.polimi.deib.se.pf02.trismvc.utils.Observer;

public class Controller implements Observer<PlayerMove> {

    private Model model;

    public Controller(Model model) {
        super();
        this.model = model;
    }

    @Override
    public void update(PlayerMove message) {
        model.move(message);
    }
}
