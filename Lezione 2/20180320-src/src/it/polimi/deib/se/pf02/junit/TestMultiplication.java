package it.polimi.deib.se.pf02.junit;

import org.junit.Assert;
import org.junit.Test;



public class TestMultiplication {
	
	@Test
	public void test(){
		Dollar five = new Dollar(5);
		Dollar product = five.times(2);
		Assert.assertEquals(10, product.getAmount());
		product = five.times(3);
		Assert.assertEquals(15, product.getAmount());
	}

}
