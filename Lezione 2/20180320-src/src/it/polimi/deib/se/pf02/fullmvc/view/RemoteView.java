package it.polimi.deib.se.pf02.fullmvc.view;

import it.polimi.deib.se.pf02.fullmvc.model.Choice;
import it.polimi.deib.se.pf02.fullmvc.model.Model;
import it.polimi.deib.se.pf02.fullmvc.model.Player;

public class RemoteView extends View {
	
	private class MessageReceiver implements Observer<String> {
		
		@Override
		public void update(String message) {		
			System.out.println("Ricevuto " + message);
			try{
				Choice choice = Choice.parseInput(message);
				processChoice(choice);
			}catch(IllegalArgumentException e){
				connection.send("Error!");			
			}		
		}
		
	}
	
	private Connection connection;
	
	public RemoteView(Player player, String enemy, Connection c) {
		super(player);
		this.connection = c;
		c.register(new MessageReceiver());
		c.asyncSend("Il tuo avversario è: " + enemy + "\nScegli la tua mossa:");
	}

	@Override
	protected void showModel(Model model) {
		connection.send(model.getOutcome(getPlayer()).toString() + "\nScegli la tua mossa:");
	}

}
