package it.polimi.deib.se.pf02.morramvc.controller;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

import it.polimi.deib.se.pf02.morramvc.model.Choice;
import it.polimi.deib.se.pf02.morramvc.model.Model;
import it.polimi.deib.se.pf02.morramvc.model.Outcome;
import it.polimi.deib.se.pf02.morramvc.view.View;

public class Controller implements Observer{
	
	private Model model;
	
	private View view;
	
	public Controller(Model model, View view) {
		this.model = model;
		this.view = view;
	}
	
	private Choice computeChoice(){
		return Choice.getRandomChoice(new Random());
	}
	
	/*
	 * Questo metodo rappresenta la vera e propria logica applicativa
	 * In realtà perte di questa è stata messa nella classe Choice nel metodo resultAgainst
	 * È possibile posizionare tutta la logica all'interno del controller.
	 */
	private void manageRound(){
		Choice computerChoice = computeChoice();
		model.setComputerChoice(computerChoice);
		Outcome outcome = model.getPlayerChoice().resultAgainst(computerChoice);
		model.setOutcome(outcome);		
	}

	@Override
	public void update(Observable o, Object arg) {
		if(o != view || !(arg instanceof Choice)){
			throw new IllegalArgumentException();
		}	
		model.setPlayerChoice((Choice) arg);
		manageRound();
	}
	
	

}
