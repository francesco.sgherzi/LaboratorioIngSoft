package it.polimi.deib.se.pf02.morramvc.view;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import it.polimi.deib.se.pf02.morramvc.model.Choice;
import it.polimi.deib.se.pf02.morramvc.model.ModelView;
import it.polimi.deib.se.pf02.morramvc.model.Outcome;

public class View extends Observable implements Runnable, Observer{
	
	private Scanner scanner;
	
	private PrintStream output;
	
	public View(InputStream inputStream, OutputStream output) {
		this.scanner = new Scanner(inputStream);
		this.output = new PrintStream(output);
	}	
	
	@Override
	public void run() {
		while(true){
			output.println("Indicare una scelta:");
			String text = scanner.next();
			try{				
				Choice choice = Choice.parseInput(text);
				setChanged();
				notifyObservers(choice);				
			}catch(IllegalArgumentException e){
				output.println("Errore di input!");
			}
		}		
	}


	private void showModel(ModelView model) {
		printPlayerChoice(model.getPlayerChoice());
		printComputerChoice(model.getComputerChoice());
		printOutcome(model.getOutcome());
	}


	private void printOutcome(Outcome outcome) {
		switch(outcome){
		case DRAW:
			print("Parità!");
			break;
		case LOSER:
			print("Hai perso!");
			break;
		case WINNER:
			print("Hai vinto!");
			break;
		default:
			throw new IllegalArgumentException();		
		}
	}

	private void print(String message) {
		output.println(message);		
	}


	private void printComputerChoice(Choice computerChoice) {
		output.println("Ho scelto " + computerChoice + ".");		
	}


	private void printPlayerChoice(Choice playerChoice) {
		output.println("Hai scelto " + playerChoice + ".");		
	}


	/*
	 * In questo caso la view interagisce direttamente col model, ricevendo direttamente le notifiche dei cambiamenti del model. 
	 * Sarebbe anche possibile gestire le interazione col model esclusivamente attraverso il controller.
	 */
	@Override
	public void update(Observable o, Object arg) {
		if(!(o instanceof ModelView)){
			throw new IllegalArgumentException();			
		}
		showModel((ModelView)o);
	}	

}
