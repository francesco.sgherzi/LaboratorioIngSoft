package it.polimi.deib.se.pf02.fullmvc.model;

public class PlayerChoice {
	
	private final Player player;
	
	private final Choice choice;

	public PlayerChoice(Player player, Choice choice) {
		this.player = player;
		this.choice = choice;
	}

	public Player getPlayer() {
		return player;
	}

	public Choice getChoice() {
		return choice;
	}	

}
