package it.polimi.deib.se.pf02.junit;

public class Dollar {
	
	private int amount = 0;
	
	public int getAmount(){
		return amount;
	}
	
	public Dollar(int value){
		amount = value;		
	}
	
	public Dollar times(int value){
		return new Dollar(value * amount);
	}

}
