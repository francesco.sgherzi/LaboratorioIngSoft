package it.polimi.deib.se.pf02.trismvc.model.message;

import it.polimi.deib.se.pf02.trismvc.model.Player;

public class ErrorMessage extends MoveMessage{

    private final String message;

    public ErrorMessage(String message, Player player) {
        super(player);
        this.message = message;
    }

    @Override
    public boolean isError() {
        return true;
    }

    @Override
    public String toString() {
        return message;
    }
}
