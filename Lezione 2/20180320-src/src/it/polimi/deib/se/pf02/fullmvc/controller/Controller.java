package it.polimi.deib.se.pf02.fullmvc.controller;

import it.polimi.deib.se.pf02.fullmvc.model.Model;
import it.polimi.deib.se.pf02.fullmvc.model.PlayerChoice;
import it.polimi.deib.se.pf02.fullmvc.view.Observer;

public class Controller implements Observer<PlayerChoice> {
	
	private Model model;

	public Controller(Model model) {
		super();
		this.model = model;
	}

	@Override
	public void update(PlayerChoice message) {
		model.setPlayerChoice(message.getPlayer(), message.getChoice());	
	}

}
