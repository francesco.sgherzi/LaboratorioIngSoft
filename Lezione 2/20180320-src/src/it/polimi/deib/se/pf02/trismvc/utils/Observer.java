package it.polimi.deib.se.pf02.trismvc.utils;

public interface Observer<T> {

    void update(T message);

}
