package it.polimi.deib.se.pf02.trismvc.model;

public class PlayerMove {

    private final Player player;

    private final int row;

    private final int column;

    public PlayerMove(Player player, int row, int column) {
        this.player = player;
        this.row = row;
        this.column = column;
    }

    public Player getPlayer() {
        return player;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

}
