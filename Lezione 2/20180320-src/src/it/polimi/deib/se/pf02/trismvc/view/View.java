package it.polimi.deib.se.pf02.trismvc.view;

import it.polimi.deib.se.pf02.trismvc.model.Player;
import it.polimi.deib.se.pf02.trismvc.model.PlayerMove;
import it.polimi.deib.se.pf02.trismvc.model.message.MoveMessage;
import it.polimi.deib.se.pf02.trismvc.model.message.SuccessMessage;
import it.polimi.deib.se.pf02.trismvc.utils.Observable;
import it.polimi.deib.se.pf02.trismvc.utils.Observer;

public abstract class View extends Observable<PlayerMove> implements Observer<MoveMessage> {

    private Player player;

    protected View(Player player) {
        this.player = player;
    }

    protected Player getPlayer(){
        return player;
    }

    void handleMove(int row, int column) {
        System.out.println(row + " " + column);
        notify(new PlayerMove(player, row, column));
    }

    protected abstract void showMessage(String message);

    @Override
    public void update(MoveMessage message)
    {
        if(message.isError() && message.getPlayer() == player){
            showMessage(message.toString() + "\nScegli la tua mossa:");
            return;
        }
        if(!message.isError()) {
            SuccessMessage moveMessage = (SuccessMessage) message;
            String resultMsg = moveMessage.getBoard().toString();
            if (moveMessage.getBoard().isGameOver(moveMessage.getPlayer().getMarker())) {
                if (message.getPlayer() == player) {
                    resultMsg = resultMsg + "\nHai vinto!";
                } else {
                    resultMsg = resultMsg + "\nHai perso!";
                }
            }
            else if(moveMessage.getBoard().isFull()){
                resultMsg = resultMsg + "\nParità!";
            }
            if(message.getPlayer() == player){
                resultMsg = resultMsg + "\nAttendi la mossa dell'altro giocatore!";
            }
            else{
                resultMsg = resultMsg + "\nScegli la tua mossa:";
            }
            showMessage(resultMsg);
        }
    }

}