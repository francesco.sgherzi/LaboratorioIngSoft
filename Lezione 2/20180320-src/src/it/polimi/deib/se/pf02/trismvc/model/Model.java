package it.polimi.deib.se.pf02.trismvc.model;


import it.polimi.deib.se.pf02.trismvc.utils.Observable;
import it.polimi.deib.se.pf02.trismvc.model.message.ErrorMessage;
import it.polimi.deib.se.pf02.trismvc.model.message.MoveMessage;
import it.polimi.deib.se.pf02.trismvc.model.message.SuccessMessage;

public class Model extends Observable<MoveMessage> {

    private Board board = new Board();

    private Cell turn = Cell.X;

    private void updateTurn(){
        if(turn == Cell.X){
            turn = Cell.O;
        }
        else{
            turn = Cell.X;
        }
    }

    public void move(PlayerMove move){
        if(move.getPlayer().getMarker() != turn){
            notify(new ErrorMessage("Non è il tuo turno!", move.getPlayer()));
            return;
        }
        if(!board.isEmpty(move.getRow(), move.getColumn())){
            notify(new ErrorMessage("La cella scelta non è vuota!", move.getPlayer()));
            return;
        }
        board.setCell(move.getRow(), move.getColumn(), move.getPlayer().getMarker());
        updateTurn();
        boolean hasWon = board.isGameOver(move.getPlayer().getMarker());
        notify(new SuccessMessage(board.clone(), move.getPlayer()));
        if(hasWon || board.isFull()){
            board.reset();
        }
    }


}
