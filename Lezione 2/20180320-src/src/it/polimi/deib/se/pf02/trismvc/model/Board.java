package it.polimi.deib.se.pf02.trismvc.model;

public class Board implements Cloneable {

    @Override
    protected final Board clone() {
        final Board result;
        try {
            result = (Board) super.clone();
        }
        catch (CloneNotSupportedException ex) {
            throw new RuntimeException("Something went wrong!", ex);
        }
        for(int i = 0; i < 3; i++){
            result.board[i] = board[i].clone();
        }
        return result;
    }

    private Cell[][] board = new Cell[3][3];

    Board(){
        reset();
    }

    public void reset(){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                board[i][j] = Cell.EMPTY;
            }
        }
    }

    public boolean isEmpty(int row, int column) {
        return row >= 0 && column >= 0 && row <= 2 && column <= 2 && board[row][column] == Cell.EMPTY;
    }

    public void setCell(int row, int column, Cell marker) {
        if(row < 0 || column < 0 || row > 2 || column > 2){
            return;
        }
        board[row][column] = marker;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++) {
                builder.append(board[i][j]);
                builder.append(" ");
            }
            builder.append("\n");
        }
        return builder.toString();
    }

    public boolean isFull(){
        for(int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == Cell.EMPTY) {
                    return false;
                }
            }
        }
        return true;
    }

    public boolean isGameOver(Cell marker){
        for(int i = 0; i < 2; i++){
            if(board[0][i] == marker && board[1][i] == marker && board[2][i] == marker){
                return true;
            }
            if(board[i][0] == marker && board[i][1] == marker && board[i][2] == marker){
                return true;
            }
        }
        return board[0][0] == marker && board[1][1] == marker && board[2][2] == marker ||
                board[0][2] == marker && board[1][1] == marker && board[2][0] == marker;
    }

}
