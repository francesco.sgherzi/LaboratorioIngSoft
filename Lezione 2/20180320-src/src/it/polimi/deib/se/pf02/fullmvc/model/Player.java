/**
 * 
 */
package it.polimi.deib.se.pf02.fullmvc.model;

/**
 * @author Alessandro
 *
 */
public class Player {
	
	private final String name;
	
	public Player(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}

}
