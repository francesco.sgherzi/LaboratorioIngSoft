package it.polimi.deib.se.pf02.trismvc.model.message;

import it.polimi.deib.se.pf02.trismvc.model.Player;

public abstract class MoveMessage{

    private final Player player;

    MoveMessage(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public abstract boolean isError();

}
