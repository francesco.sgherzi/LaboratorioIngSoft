package it.polimi.deib.se.pf02.trismvc.view;

import it.polimi.deib.se.pf02.trismvc.model.Player;
import it.polimi.deib.se.pf02.trismvc.server.ClientConnection;
import it.polimi.deib.se.pf02.trismvc.utils.Observer;

public class RemoteView extends View {

    private class MessageReceiver implements Observer<String> {

        @Override
        public void update(String message) {
            System.out.println("Ricevuto " + message);
            try{
                String[] inputs = message.split(",");
                handleMove(Integer.valueOf(inputs[0]), Integer.valueOf(inputs[1]));
            }catch(IllegalArgumentException e){
                clientConnection.asyncSend("Error!");
            }
        }

    }

    private ClientConnection clientConnection;

    public RemoteView(Player player, String enemy, ClientConnection c) {
        super(player);
        this.clientConnection = c;
        c.register(new MessageReceiver());
        c.asyncSend("Il tuo avversario è: " + enemy + "\nScegli la tua mossa:");

    }

    @Override
    protected void showMessage(String message) {
        clientConnection.asyncSend(message);
    }

}