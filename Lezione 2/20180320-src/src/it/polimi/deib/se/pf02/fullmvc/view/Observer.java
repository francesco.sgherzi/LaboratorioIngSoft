package it.polimi.deib.se.pf02.fullmvc.view;

public interface Observer<T> {

	public void update(T message);
	
}
