package it.polimi.deib.se.pf02.trismvc.model.message;

import it.polimi.deib.se.pf02.trismvc.model.Board;
import it.polimi.deib.se.pf02.trismvc.model.Player;

public class SuccessMessage extends MoveMessage {
    
    private final Board board;

    public SuccessMessage(Board board, Player player) {
        super(player);
        this.board = board;
    }

    @Override
    public boolean isError() {
        return false;
    }

    public Board getBoard() {
        return board;
    }

}
