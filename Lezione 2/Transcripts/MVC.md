# MVC

## Model
-> Notifica view dei camviamenti

 * Incapsula lo stato dell’applicazione
 * I Permette di accedere ai dati
 * I Notifica i cambiamenti dello stato

## View
-> notifica azioni utente a controller

-> Richiede informazioni dal Model
  * Mostra il modello
  * I Gestisce l’interazione con l’utente

## Controller
-> seleziona vista dalla View
-> Modifica lo stato della View
 * Rappresenta la logica applicativa
 * I Collega le azioni dell’utente con modifiche allo stato
 * I Sceglie cosa deve essere mostrato


## Come si realizza
 * La comunicazione è gestita dalla programmazione ad eventi
 * Un oggetto sorgente genererà oggetti eventi
 * Un oggetto ascoltatore registrato alla sorgente verrà notificato dagli eventi
 * Concetto simile a Twitter



### Eventi

 * Azioni generalmente dell’utente
 * I Notifiche da model e controller
 * I Sono delle classi e contengono delle informazioni dettagliate
sull’evento

### Ascoltatori (listener)
 * Si mettono in ascolto di un evento
 * I Devono avere dei metodi per poter reagire agli eventi
 * I Possono esserci più ascoltatori per un evento



 ### Sorgenti di eventi
  * I Notificano gli eventi agli interessati
  * I La notifica avviene invocando i metodi sugli ascoltatori
  * I Devono avete un metodo per permettere la registrazione degli ascoltatori

### Due interfacce per la comunicazione
 * Separazione logica applicativa e interfaccia
 * Si utilizzano due interfacce:
 * I Controller-Model/View: aggiorna la visualizzazione
 * I View/Controller-Model: invia comandi e richieste



### Passaggio di oggetti
 * Sempre oggetti che rappresentano eventi o creati
appositamente
 * I Oggetti modificabili del modello non devono arrivare
all’interfaccia


### Soluzioni
 * I Interfacce limitate (solo metodi per la visualizzazione delle
informazioni selezionate)
 * I Oggetti immutabili
 * I Oggetti creati appositamente per la visualizzazione
.
