# jUnit 4 - testing

## Unit testing
Divido il progetto in unità che testo singolarmente, simulando l'ambiente circostante.
Importante perché posso testare parti del programma anche se il programma non è finito.

Ogni componente viene testato separatamente (sia classe o metodo)

**Importante, ogni test deve avere almeno due test al suo interno, uno risultante positivo e uno negativo**

Si usano le assertion per confrontare i risultati con i valori attesi.



## White Box vs Black Box

* White Box assume che già si abbia il codice
* Black Box testa solo input e output, considerando il pezzo di codice da testare come, appunto, un Black Box


## Due features

 * Fixture -> setto l'ambiente prima del test, con ```@Before``` e ```@After``` posso dichiarare che metodi / codice eseguire prima e dopo il test per pulizia / setup. Con ```@Test``` codice dei casi di test veri e propri
   * ```@Before``` codice eseguito prima
   * ```@After``` codice eseguito dopo
   * ```@Test``` codice del test
 * Test runner -> esegire test in maniera trasparente all'utente



# Le asserzioni
* ```assertEquals(expected, actual)``` -> testo l'uguaglianza
* ```assertSame``` -> identità (riferimento a oggetto uguale, cioè sono lo stesso oggetto)
* ```assertTrue``` / ```assertFalse``` -> testo condizioni booleane


## Regole per un buon unit test
 * Ogni test copre una e una sola funzionalità
 * Ogni test deve essere automatizzato
 * l’insieme dei casi di test deve coprire tutte le funzionalità dell’unità

## Best practice
 * non utilizzare il costruttore del test case per settare il test case
 * non assumere di conoscere l’ordine nel quale i test case vengono eseguiti (anche all’interno del singolo jUnit)
 * non scrivere test case che abbiano side effect
 * scrivere i test che leggono dati da locazioni del file system utilizzando path relativi
 * memorizzare i dati che sono necessari per i test assieme ai test stessi
 * assicurarsi che i nomi dei test siano time-independent
 * scegliere i nomi dei test nel modo corretto:
 * il nome del test deve iniziare con la parola Test (esempioTestClassUnderTest)
  * il nome dei metodi nel test case devono descrivere cosa viene testato (esempio testLoggingEmptyMe
 * mantenere i test piccoli e veloci

## Come testare i metodi privati
 * è possibile testare metodi privati tramite i metodi pubblici che li utilizzano
 * alternativamente è possibile utilizzare la reflection

## Quanto testare
In genere tanto più una parte di codice è visibile dall’esterno, tanto più deve essere testata




























.
