package trismvc.model;

public class Board {

    private Cell[][] cells = new Cell[3][3];

    public Board(){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++) {
                cells[i][j] = Cell.EMPTY;
            }
        }
    }

    public boolean isEmpty(int r, int c){
        if(r < 0 || r> 2 || c < 0 || c > 2) return false;
            return cells[r][c] == Cell.EMPTY;
    }

    public void set(int r, int col, Cell c){
        cells[r][col] = c;
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder();

        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                message.append(cells[i][j]);
            }
            message.append("\n");
        }
        return message.toString();
    }
}
