package trismvc.model;

public enum Cell {
    EMPTY(" "), X("X"), O("O");

    private final String message;

    Cell(String message){
        this.message = message;
    }

    @Override
    public String toString(){
        return message;
    }

}
