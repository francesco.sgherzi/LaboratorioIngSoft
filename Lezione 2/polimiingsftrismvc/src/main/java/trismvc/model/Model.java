package trismvc.model;

import trismvc.messages.PlayerMove;
import trismvc.utils.Observable;

public class Model extends Observable<Board>{

    private Board board;

    public Board getBoard() {
        return board;
    }

    private Cell turn = Cell.X;

    public void move(PlayerMove move){
        board.set(move.getRow(), move.getColumn(), move.getCell());
        this.notify(board);
    }

    public Cell getTurn() {
        return turn;
    }

    public void nextTurn(){
        if(turn == Cell.X) turn = Cell.O;
        if(turn == Cell.O) turn = Cell.X;

    }

}
