package trismvc.controller;

import trismvc.messages.PlayerMove;
import trismvc.model.Model;
import trismvc.utils.Observer;

public class Controller implements Observer<PlayerMove>{

    public Controller(Model model) {
        this.model = model;
    }

    private Model model;

    public void update(PlayerMove message) {
        if(message.getCell() == model.getTurn()){
            if(model.getBoard().isEmpty(message.getRow(), message.getColumn())) {
                model.move(message);
            }
        }
    }
}
