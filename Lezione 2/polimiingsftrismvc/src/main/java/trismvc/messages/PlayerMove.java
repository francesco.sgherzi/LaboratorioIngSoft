package trismvc.messages;

import trismvc.model.Cell;

public class PlayerMove {
    private Cell cell;

    private int row;
    private int column;

    public PlayerMove(Cell cell, int row, int column) {

        this.cell = cell;
        this.row = row;
        this.column = column;
    }

    public Cell getCell() {
        return cell;
    }

    public void setCell(Cell cell) {
        this.cell = cell;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }
}
