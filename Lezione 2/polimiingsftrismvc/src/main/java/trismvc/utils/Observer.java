package trismvc.utils;

public interface Observer <T> {
    public void update(T message);
}
