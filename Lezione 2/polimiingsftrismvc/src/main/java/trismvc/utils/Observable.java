package trismvc.utils;

import java.util.ArrayList;

public class Observable <T>{

    private ArrayList<Observer<T>> observers = new ArrayList<Observer<T>>();

    public void register(Observer<T> listener){
        synchronized (observers){
            observers.add(listener);
        }
    }

    public void deregister(Observer<T> listener){
        synchronized (observers){
            observers.remove(listener);
        }
    }

    protected void notify(T message){
        synchronized (observers){
            for( Observer<T> o: observers){
                o.update(message);
            }
        }
    }

}
