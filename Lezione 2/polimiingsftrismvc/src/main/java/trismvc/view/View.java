package trismvc.view;

import trismvc.messages.PlayerMove;
import trismvc.model.Board;
import trismvc.model.Cell;
import trismvc.server.SocketClientConnection;
import trismvc.utils.Observable;
import trismvc.utils.Observer;

public class View extends Observable<PlayerMove> implements Observer<Board>{
    private Cell cell;
    private SocketClientConnection connection;

    public View(Cell cell, SocketClientConnection connection) {
        this.cell = cell;
        this.connection = connection;
        this.connection.register(new ConnectionHandler());
    }

    private void handleMove(PlayerMove move){
        this.notify(move);
    }

    public void update(Board message) {
        this.connection.send(message.toString());
    }

    private class ConnectionHandler implements Observer<String>{

        public void update(String message) {
            String[] values = message.trim().split(",");
            int row = Integer.valueOf(values[0]);
            int column = Integer.valueOf(values[1]);
            handleMove(new PlayerMove(cell, row, column));
        }

    }
}
