package trismvc.server;

import trismvc.utils.Observable;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

public class SocketClientConnection extends Observable<String> implements Runnable {

    private Socket socket;
    private Server server;
    private PrintStream out;
    private boolean connected = true;

    public SocketClientConnection(Socket socket, Server server) {
        this.socket = socket;
        this.server = server;
    }

    public void send(String message){
        out.println(message);
        out.flush();
    }

    private void close() {
        this.closeConnection();
        this.server.deregisterConnection(this);
    }

    public synchronized void closeConnection(){
        this.send("Connessione terminata");
        try {
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        Scanner in;
        try {
            in = new Scanner(socket.getInputStream());
            out = new PrintStream(socket.getOutputStream());
            this.send("Benvenuto, chi sei?");
            String name = in.nextLine();
            server.rendezvous(this, name);

            while (this.connected){
                String line = in.nextLine();
                this.notify(line);
            }

        } catch (IOException e) {
            System.out.println("Connection error");
            e.printStackTrace();
        } finally {
            this.close();
        }
    }
}
