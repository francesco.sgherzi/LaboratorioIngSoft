package trismvc.server;

import trismvc.controller.Controller;
import trismvc.model.Cell;
import trismvc.model.Model;
import trismvc.view.View;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

public class Server {

    private ServerSocket serverSocket;

    private ArrayList<SocketClientConnection> waitingPlayers =
            new ArrayList<SocketClientConnection>();

    private HashMap<SocketClientConnection, SocketClientConnection> playingConnection =
            new HashMap<SocketClientConnection, SocketClientConnection>();

    public Server() throws IOException {
        serverSocket = new ServerSocket(12345);
    }

    public void rendezvous (SocketClientConnection connection, String name){
        waitingPlayers.add(connection);
        if(waitingPlayers.size() == 2){
            SocketClientConnection p1 = waitingPlayers.get(0);
            SocketClientConnection p2 = waitingPlayers.get(1);
            waitingPlayers.clear();
            playingConnection.put(p1, p2);
            playingConnection.put(p2, p1);
            // Creo la partita
            Model model = new Model();
            View v1 = new View(Cell.X, p1);
            View v2 = new View(Cell.X, p2);

            Controller controller = new Controller(model);

            v1.register(controller);
            v2.register(controller);
            model.register(v1);
            model.register(v2);

        }
    }

    public void deregisterConnection (SocketClientConnection connection){
        SocketClientConnection avversario = playingConnection.get(connection);
        if(avversario != null){
            playingConnection.remove(avversario);
            playingConnection.remove(connection);
            avversario.closeConnection();
        } else {
            waitingPlayers.remove(connection);
        }
    }


    public void run(){
        while(true){
            try {
                Socket newSocket = this.serverSocket.accept();
                new SocketClientConnection(newSocket, this);
            } catch (IOException e) {
                System.out.println("Connection error!");
                e.printStackTrace();
            }
        }
    }

    public static void main(String args[]){
        Server server = null;
        try {
            server = new Server();
            server.run();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
