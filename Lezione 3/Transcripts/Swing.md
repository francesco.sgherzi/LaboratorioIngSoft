# Swing
 -> forniscono la stessa interfaccia grafica su tutte le piattaforme
 ## Temi
 Forniscono diversi temi, chiamati look and feel
 Si selezionano con
 *  `static void setLookAndFeel(String className)`
 *  `static void setLookAndFeel(LookAndFeel newLookAndFeel)`

Tutto è un file... ehm un oggetto... ehm un Component (COFF COFF REACT NATIVE) ehm... un Container

## Component
Component è una interfaccia astratta e rappresenta l'entità di un oggetto grafico col quale l'utente può interagire.

## Container
Il container rappresenta un contenitore: posso vedere la finestra come contenitore di altri componenti (testo, bottone, ecc.)
Un container può contenere altri container.
Il container si divide in Window e JComponent.

### Top level Container
* JFrame
* JApplet serve per racchiudere finestra di un applet java (in web) **NON USARE MAI**
* JDialog

Questi tre sono obbligatori, gli altri no. Sono comunque utilizzati per creare gerarchie.
#### Gerarchia di contenimento
* root pane &rarr; layer principale che gestisce gli altri layer
* layered pane &rarr; come il content pane, ma contiene anche la barra dei menu
* content pane &rarr; consente di inserire e visualizzare gli altri componenti
* glass pane &rarr; posto davanti agli altri layer, non è visibile e serve per compiere azioni
## JComponent
Rappresenta il capostipite dei controlli grafici di Swing, tutti i controlli fanno riferimento a questa classe.
exempli gratia:
* JText
* JComboBox
* JList
* JButton
* etc...

### Window
Window è una finsetra senza niente, né bordi nè menu. Si possono inserire widget e altri contenitori.
Questa classe è estesa da frame e dialog.

#### Frame (JFrame)
Il più importante top level container; corrisponde, in se, ad una finestra vuota.
Crea una finestra con titolo, bordi, icona e i pulsanti di finestra.

Un esempio d'uso:
```java
package swing;
import javax.swing.JFrame;
public class SwingJFrame{
  public static void main(String[] args){
    JFrame window = new JFrame();
    // decido le dimensioni della finestra
    window.setSize(500, 600);
    // rendo visibile la finestra
    window.setVisible(true);
    // scelgo che cosa succedera quando si chiudera la finestra
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    // centro la finestra nello schermo
    window.setLocationRelativeTo(null);
  }
}
```

#### Dialog (JDialog)
Serve per proporre una certa interazione con l'utente.


**NB le classi di Swing si chiamano come quelle di awt, ma con una J davanti**

## Layout
Per effettuare il layout è possibile specificare la posizione di ogni container oppure si può utilizzare un `LayoutManager` per facilitare l'operazione e rendere l'interfaccia responsive.

Ci sono diversi LayoutManager:
### FlowLayout
Ordina orizzontalmente i componenti finchè non finisce lo spazio, poi va a capo.

```java

```

### BorderLayout
Divide l'area in 5 sottoaree

```
----------------------------------
|          PAGE_START            |
----------------------------------
|          |            |        |
|          |            |        |
|LINE_START|   CENTER   |LINE_END|
|          |            |        |
|          |            |        |
----------------------------------
|          PAGE_END              |
----------------------------------
```



* `PAGE_START`
* `PAGE_END`
* `LINE_START`
* `LINE_END`
* `CENTER`


Posso definire anche dei gap con `HORIZONTAL GAP` e `VERTICAL GAP`


### GridLayout
Definisce una griglia di elementi dove si possono mettere i componenti, che devono essere tutti della stessa dimensione.

### GrdiBagLayout
Più complessa da utilizzare ma permette più flessibilità: un componente può espandersi su più celle e le righe/colonne possono avere altezze/larghezze diverse.

Alcuni attributi:
* `gridx`, `gridy`: posizione del componente (in righe/colonne).
* `gridwidth`, `gridheight`: dimensione del componente (in righe/colonne).
* `fill`: come riempire lo spazio aggiuntivo, valori possibili: `HORIZONTAL`, `VERTICAL`, `NONE`, `BOTH`.
* `ipadx`, `ipady`: quanto spazio aggiungere al componente (in Pixel).
* `insets`: spaziatura tra gli altri componenti (oggetto Insets).
* `anchor`: se il componente è più piccolo dello spazio riservato indica come deve essere posizionato. Possibili valori `CENTER` (default), `PAGE_START`, `PAGE_END`, `LINE_START`, `LINE_END`, `FIRST_LINE_START`, `FIRST_LINE_END`, `LAST_LINE_END`, `LAST_LINE_START`.
* `weightx`, `weighty`: servono a distribuire eventuale spazio avanzato su una riga/colonna.

```
```

### Layout annidati
E' necessario un JPanel &rarr; fornisce un semplice contenitore general purpose.


**I componenti possono anche non avere le stesse dimensioni**



















 .
