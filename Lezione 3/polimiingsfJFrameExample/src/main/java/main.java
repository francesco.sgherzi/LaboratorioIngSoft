import javax.swing.*;

public class main {
    public static void main(String args[]) {
        JFrame window = new JFrame();
        window.setSize(400, 400);
        window.setVisible(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLocationRelativeTo(null);
    }
}
